import React from 'react';
import ReactDOM from 'react-dom';

import App from './app';

ReactDOM.render(<App name="Harvey"/>, document.getElementById('react-app'));
