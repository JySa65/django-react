var path = require('path');
var webpack = require('webpack');
var BundleTracker = require('webpack-bundle-tracker');

module.exports = {
    context: __dirname,
    entry: './assets/js/index',
    output: {
        filename: '[name]-[hash].js',
        path: path.resolve('./assets/bundles/'),
        publicPath: 'http://localhost:8080/assets/bundles/'
    },
    plugins: [
        new BundleTracker({filename: './webpack-stats.json'})
    ],
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    },
    resolve: {
        modules: ['node_modules'],
        extensions: ['.js', '.jsx']
    },
    devServer: {
        contentBase: path.resolve('./assets/bundles/'),
        publicPath: 'http://localhost:8080/assets/bundles/',
        host: "0.0.0.0"
    }
};
